// Return codes:
// 0: ok
// 1: internal error
// 2: missing configuration values
// 3: unexpected response from Gitlab Server
package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"log/slog"
	"net/http"
	"os"
	"os/user"
	"path/filepath"
	"strings"
)

const (

	// configurable
	keyContext = "context"
	keyProject = "project"
	keyServer  = "server"
	keyToken   = "private_token"

	defaultFilename = ".gitlab-ci.yml"
	envPrefix       = "LINT_GITLAB_CI_"
	configPath      = "~/.gitlab"
	configFilename  = "config"
	comment         = "#"
	defaultContext  = "/api/v4"
	statusValid     = "valid"
)

var keys = []string{keyContext, keyProject, keyServer, keyToken}

var (
	// Version is set externally via linker
	Version string

	// Commit is set externally via linker
	Commit string
)

type Config struct {
	context string
	project string
	server  string
	token   string
}

func (a *Config) Set(key string, value string) {
	if len(value) == 0 {
		return
	}
	switch key {
	case "context":
		a.context = value
	case "project":
		a.project = value
	case "server":
		a.server = value
	case "token":
		a.token = value
	default:
		fmt.Fprintf(os.Stderr, "ignoring unknown key %q\n", key)
	}
}

func (a *Config) fromEnv(prefix string, suffix []string) {
	for i := range suffix {
		k := prefix + strings.ToUpper(suffix[i])
		if v, ok := os.LookupEnv(k); ok {
			a.Set(suffix[i], v)
		}
	}
}

func (a *Config) fromFile(filename string) {
	kvs, err := readConfig(filename)
	if err != nil {
		return
	}
	for k, v := range kvs {
		a.Set(k, v)
	}
}

func (cfg Config) Validate() error {
	var k string
	if len(cfg.context) == 0 {
		k = "context"
	} else if len(cfg.project) == 0 {
		k = "project"
	} else if len(cfg.server) == 0 {
		k = "server"
	} else if len(cfg.token) == 0 {
		k = "token"
	}
	if len(k) != 0 {
		return fmt.Errorf("missing property %s", k)
	}
	return nil
}

func (cfg Config) endpointURL() string {
	// https://gitlab.example.com/api/v4/projects/:id/ci/lint
	return fmt.Sprintf("https://%s/%s/projects/%s/ci/lint?private_token=%s", cfg.server, cfg.context, cfg.project, cfg.token)
}

func mask(secret, message string) string {
	return strings.ReplaceAll(message, secret, "***")
}

// Very simple configuration store, no json, no yaml, no toml, no properties, no
// nothing.
// Basically the same format as environment variables, line based key=value
// hashtag ('#') may start a comment in column 0
// whitespace is ignored
// empty lines are ignored
func readConfig(filename string) (map[string]string, error) {
	m := make(map[string]string)
	f, err := os.Open(filename)
	if err != nil {
		return m, err
	}
	defer f.Close()

	// line numbers start from 1, just as in vim
	lineno := 0
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		lineno++
		line := strings.TrimSpace(sc.Text())
		if len(line) == 0 {
			continue
		}
		if strings.HasPrefix(line, comment) {
			// ignore line, continue with next line
			continue
		}
		fields := strings.Split(line, "=")
		if len(fields) != 2 {
			fmt.Fprintf(os.Stderr,
				"error in %s line #%d: expected key=value, got %q\n",
				filename, lineno, line)
			continue
		}
		key := strings.TrimSpace(fields[0])
		value := strings.TrimSpace(fields[1])
		m[key] = value
	}
	return m, nil
}

type LintRequest struct {
	Content string `json:"content"`
}

type LintResponse struct {
	Valid      bool
	Errors     []string
	Warnings   []string
	MergedYaml string `json:"merged_yaml"`
}

// home returns the value of the HOME environment variable.
// If HOME is unset, it will default to the operating system's opinion of where
// to find the current user's HOME directory.
// The location is used to read default Gitlab configuration settings.
func Home() (string, error) {
	s := os.Getenv("HOME")
	// empty if not existent
	if len(s) > 0 {
		return s, nil
	}
	usr, err := user.Current()
	if err != nil {
		return s, err
	}
	return usr.HomeDir, nil
}

func main() {
	// Pass 1: config from local file
	cfg := Config{}
	// Local config file in ${HOME}?
	h, err := Home()
	if err != nil {
		log.Fatalf("error determining HOME directory: %v", err)
	}
	absConfigFilename := filepath.Join(h, ".gitlab", configFilename)
	cfg.fromFile(absConfigFilename)

	// Pass 2: config from environment variables
	cfg.fromEnv(envPrefix, keys)

	// Pass 3: config from commandline parameter
	context := flag.String(keyContext, defaultContext, "Gitlab context URL for REST API")
	project := flag.String(keyProject, "", "Gitlab project ID")
	server := flag.String(keyServer, "", "Gitlab lint CI server")
	token := flag.String(keyToken, "", "Gitlab private token")

	filename := flag.String("filename", ".gitlab-ci.yml", "input file to lint")
	debug := flag.Bool("debug", false, "debug log level")
	flag.Parse()

	if *debug {
		// Haven't found a way to directly write to slog.Default(), it only exports Handler() no options
		// slog.Default().HandlerOptions.Level = slog.LevelDebug
		logger := slog.New(slog.NewTextHandler(log.Default().Writer(), &slog.HandlerOptions{Level: slog.LevelDebug}))
		slog.SetDefault(logger)
	}

	cfg.Set(keyContext, *context)
	cfg.Set(keyProject, *project)
	cfg.Set(keyServer, *server)
	cfg.Set(keyToken, *token)

	if err := cfg.Validate(); err != nil {
		log.Fatal(err)
	}

	u := cfg.endpointURL()
	// Use private_token as parameter instead of HTTP Header
	slog.Debug("using endpoint", "url", mask(cfg.token, u))

	// Prepare request payload
	// spec: https://docs.gitlab.com/ee/api/lint.html
	buf, err := ioutil.ReadFile(*filename)
	if err != nil {
		log.Fatal(err)
	}

	req := LintRequest{Content: string(buf)}
	js, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}
	res, err := http.Post(u, "application/json", bytes.NewReader(js))
	if err != nil {
		log.Fatal(err)
	}
	if http.StatusOK != res.StatusCode {
		fmt.Fprintf(os.Stderr, "want status code %d but got %d\n", http.StatusOK, res.StatusCode)
		fmt.Fprintf(os.Stderr, "response: %+v\n", res)
		os.Exit(3)
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	slog.Debug("received", "response body", string(body))
	res.Body.Close()

	// Try to parse JSON result
	var result LintResponse
	if err := json.Unmarshal(body, &result); err != nil {
		// mehhh - structure change, cannot look inside
		fmt.Fprintf(os.Stderr, "structure change, cannot look inside\n")
		fmt.Println(string(body))
		os.Exit(4)
	}

	// show result

	if result.Valid {
		fmt.Printf("file %q is valid\n", *filename)
	} else {
		fmt.Printf("file %q is NOT valid\n", *filename)
	}
	// Show optional errors
	if len(result.Errors) > 0 {
		fmt.Println("\nErrors:")
		for i, s := range result.Errors {
			fmt.Printf("    #%d: %s\n", i, s)
		}
	}
	// Show optional warnings
	if len(result.Warnings) > 0 {
		fmt.Println("\nWarnings:")
		for i, s := range result.Warnings {
			fmt.Printf("    #%d: %s\n", i, s)
		}
	}
	// Show merged YAML
	if *debug {
		fmt.Println()
		fmt.Println(result.MergedYaml)
	}
}
