
version ?= $(subst v,,$(shell git describe --tags))
commit ?= $(shell git rev-parse --short HEAD)

.PHONY: all
all: clean tidy build test install

.PHONY: clean
clean:
	go clean

.PHONY: tidy
tidy:
	go vet ./...
	# does not support Go 1.21 as of 22.08.2023
	# staticcheck ./...

.PHONY: test
test:
	go test ./...

.PHONY: build
build:
	go build ./...

.PHONY: install
install: lint-gitlab-ci

lint-gitlab-ci:
	go install -ldflags "-X main.Version=$(version) -X main.Commit=$(commit)" ./...

